<?php

Class Color {

  private $red = null;
  private $green = null;
  private $blue = null;

  public function __construct()
  {
    $numArgs = func_num_args();
    switch($numArgs) {
      case 1:
        $input = func_get_arg(0);
        if (is_a($input, "Color"))
        {
          $this->red = $input->getRed();
          $this->green = $input->getGreen();
          $this->blue = $input->getBlue();
          return;
        }
        if (!is_string($input))
          throw new Exception('Invalid input for constructor');

        if ($input[0] == '#')
        {
          if (!ctype_xdigit(substr($input, 1)) || (strlen($input) != 7))
            throw new Exception('Invalid CSS String');
          $hexRed = hexdec(substr($input, 1,2));
          $hexGreen = hexdec(substr($input, 3,2));
          $hexBlue = hexdec(substr($input, 5,2));
          // This would be a simple for loop if the colors were in an array
          self::isValidRGB($hexRed);
          self::isValidRGB($hexGreen);
          self::isValidRGB($hexBlue);
          $this->red = $hexRed;
          $this->green = $hexGreen;
          $this->blue = $hexBlue;
          return;
        } else {
          throw new Exception('Invalid css-style rgb string');
        }
        break;
      case 3:
        for ($i = 1; $i < $numArgs; $i++)
        {
          self::isValidRGB(func_get_arg($i));
        }
        $this->red = func_get_arg(0);
        $this->green = func_get_arg(1);
        $this->blue = func_get_arg(2);
        break;
      default:
        $this->red = self::getRandomRGB();
        $this->green = self::getRandomRGB();
        $this->blue = self::getRandomRGB();
    }
  }
  public function setRed($val)
  {
    self::isValidRGB($val);
    $this->red = $val;
  }

  public function setGreen($val)
  {
    self::isValidRGB($val);
    $this->green = $val;
  }

  public function setBlue($val)
  {
    self::isValidRGB($val);
    $this->blue = $val;
  }

  public function getRed()
  {
    return $this->red;
  }

  public function getGreen()
  {
    return $this->green;
  }

  public function getBlue()
  {
    return $this->blue;
  }

  public function getCSSColor()
  {
    return '#' .  self::padHex(dechex($this->red)) . self::padHex(dechex($this->green)) . self::padHex(dechex($this->blue));
  }

  private static function isValidRGB($val)
  {
    if (($val >= 0) && ($val <= 255))
    {
      return;
    }
    throw new Exception("$val is not a valid RBG Value");
  }

  public static function getRandomRGB()
  {
    return rand(0,255);
  }

  private static function padHex($val)
  {
    return str_pad($val, 2, '0', STR_PAD_LEFT);
  }

  private function getColorValueByName($name)
  {
    switch($name)
    {
      case 'red':
        return $this->red;
        break;
      case 'green':
        return $this->green;
        break;
      case 'blue':
        return $this->blue;
        break;
      default:
        throw new Exception("Color $name not a valid color");
    }
  }

  private function setColorValueByName($name, $val)
  {
    switch($name)
    {
      case 'red':
        self::setRed($val);
        break;
      case 'green':
        self::setGreen($val);
        break;
      case 'blue':
        self::setBlue($val);
        break;
      default:
        throw new Exception("Color $name is not a valid color");
    }
  }

  public function increaseColorByPercent($color, $percent)
  {
    if (($percent < 1) || ($percent > 100))
      throw new Exception('Percent must be between 1 and 100.');
    $cColor = $this->getColorValueByName($color);
    $newVal = $cColor * (1 + ($percent / 100));
    if ($newVal > 255)
      $newVal = 255;
    self::setColorValueByName($color, (int)$newVal);
  }

}
