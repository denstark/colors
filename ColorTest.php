<?php

require_once(__DIR__ . '/Color.php');

$colorRgb = new Color(12, 13, 15);
$colorHex = new Color(0x52,0x8B,0xFF);
$colorCss = new Color('#ffcc00');
$colorRgbCopy = new Color($colorRgb);
$colorRand = new Color();

echo "RGB test\n";
echo (12 == $colorRgb->getRed()) ? '.' : 'F';
echo (13 == $colorRgb->getGreen()) ? '.' : 'F';
echo (15 == $colorRgb->getBlue()) ? '.' : 'F';
echo ('#0c0d0f' == $colorRgb->getCSSColor()) ? '.' : 'F';
echo "\n\n";

echo "Hex test\n";
echo (82 == $colorHex->getRed()) ? '.' : 'F';
echo (139 == $colorHex->getGreen()) ? '.' : 'F';
echo (255 == $colorHex->getBlue()) ? '.' : 'F';
echo ('#528bff' == $colorHex->getCSSColor()) ? '.' : 'F';
echo "\n\n";

echo "CSS test\n";
echo (255 == $colorCss->getRed()) ? '.' : 'F';
echo (204 == $colorCss->getGreen()) ? '.' : 'F';
echo (0 == $colorCss->getBlue()) ? '.' : 'F';
echo ('#ffcc00' == $colorCss->getCSSColor()) ? '.' : 'F';
echo "\n\n";

echo "Copy test\n";
echo (12 == $colorRgbCopy->getRed()) ? '.' : 'F';
echo (13 == $colorRgbCopy->getGreen()) ? '.' : 'F';
echo (15 == $colorRgbCopy->getBlue()) ? '.' : 'F';
echo ('#0c0d0f' == $colorRgbCopy->getCSSColor()) ? '.' : 'F';
echo "\n\n";

echo "Random test\n";
echo ($colorRand->getRed() >= 0 && $colorRand->getRed() <= 255) ? '.' : 'F';
echo ($colorRand->getGreen() >= 0 && $colorRand->getGreen() <= 255) ? '.' : 'F';
echo ($colorRand->getBlue() >= 0 && $colorRand->getBlue() <= 255) ? '.' : 'F';
echo "\n\n";

echo "Setter test\n";
$colorRgb->setRed(13);
$colorRgb->setGreen(15);
$colorRgb->setBlue(17);

echo (13 == $colorRgb->getRed()) ? '.' : 'F';
echo (15 == $colorRgb->getGreen()) ? '.' : 'F';
echo (17 == $colorRgb->getBlue()) ? '.' : 'F';
echo ('#0d0f11' == $colorRgb->getCSSColor()) ? '.' : 'F';
echo "\n\n";

echo "Invalid test\n";

try
{
  new Color(12, -1, 15);
  echo 'F';
}
catch (\Exception $ex)
{
  echo '.';
}

try
{
  new Color('#ffgg00');
  echo 'F';
}
catch (\Exception $ex)
{
  echo '.';
}

try
{
  new Color('fc0');
  echo 'F';
}
catch (\Exception $ex)
{
  echo '.';
}

try
{
  new Color('#fc0');
  echo 'F';
}
catch (\Exception $ex)
{
  echo '.';
}

try
{
  new Color(new stdClass());
  echo 'F';
}
catch (\Exception $ex)
{
  echo '.';
}

echo "\n\n";